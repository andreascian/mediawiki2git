# mediawiki2git

Simple Python script to convert mediawiki article history to git commits

This allows for easy blame on an article, which is something that MW does NOT provide (yet ;-) out of the box

The script creates a sub-directory with the name of the article and there:
* it creates a brand new git repository
* get each single version of the page and
  * write the page content
  * create a new commit with the changes

After finishing creating the repository you can use the standard git command to search for changes (e.g. `git log -p` or `git blame`)

# Installation

For development, I commonly use `virtualenv` and install requirements with the following commands:

```bash
virtualenv -p /usr/bin/python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

# Usage

Just check `--help`

```
usage: wikiarticle2git.py [-h] [--site SITE] [--username USERNAME] [--password PASSWORD] [--protocol {http,https}] article

Fetch Mediawiki article revisions and store in a Git repo.

positional arguments:
  article               Name of the Mediawiki article

options:
  -h, --help            show this help message and exit
  --site SITE           URL of the Mediawiki site
  --username USERNAME   Username for Mediawiki site
  --password PASSWORD   Password for Mediawiki site
  --protocol {http,https}
                        Protocol to use for connecting to the Mediawiki site
```

E.g.

```bash
python wikiarticle2git.py --site wiki.dave.eu Main_Page
cd wiki.dave.eu
git blame Main_Page.wiki
```

For a private wiki, which requires username and password:

```bash
python wikiarticle2git.py --site davewiki.lan.dave.eu --username MyUsername MainPage
```

The script will ask for password from stdin
