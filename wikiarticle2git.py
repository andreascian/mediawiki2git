import os
import time
import mwclient
import getpass

from git import Repo
from git import Actor

import argparse

parser = argparse.ArgumentParser(description='Fetch Mediawiki article revisions and store in a Git repo.')
parser.add_argument('article', type=str, help='Name of the Mediawiki article')
parser.add_argument('--site', type=str, default='https://wiki.dave.eu',
                    help='URL of the Mediawiki site')
parser.add_argument('--username', type=str, default='', help='Username for Mediawiki site')
parser.add_argument('--password', type=str, default='', help='Password for Mediawiki site')
parser.add_argument('--protocol', type=str, default='https',
                    choices=['http', 'https'],
                    help='Protocol to use for connecting to the Mediawiki site')


args = parser.parse_args()
article = args.article
site_url = args.site
username = args.username
password = args.password
protocol = args.protocol

if username != '':
    password = getpass.getpass('Enter password: ')

start_time = '2011-01-01T00:00:00Z'

file_name = article + ".wiki"
# replace various unwanted characters
file_name = file_name.replace(' ', '_')
file_name = file_name.replace('/', '_')

repo_dir = os.path.join(os.getcwd(), site_url)
repo = Repo.init(repo_dir)

content_path = os.path.join(repo_dir, file_name)

site = mwclient.Site((protocol, site_url), path='/')

if username and password:
    site.login(username, password)

page = site.Pages[article]

print("repository created, now start getting revisions")

for rev in page.revisions(start=start_time, limit=50, dir='newer', prop='ids|timestamp|flags|comment|user|content'):

    content = rev['*']
    # convert mw time to timestamp
    timestamp = time.strftime('%Y-%m-%d %H:%M:%S', rev['timestamp'])

    comment = rev['comment']
    if len(comment) == 0:
        comment = "blank comment"

    print("processing revision for " + timestamp)

    f = open(content_path, 'wb')
    f.write(content.encode('utf-8'))
    f.close()

    index = repo.index
    index.add([file_name])

    actor = Actor(rev['user'].encode('utf-8'), "wiki2git@dave.eu")

    index.commit(comment, author=actor, committer=actor, commit_date=timestamp, author_date=timestamp)

print("done")
